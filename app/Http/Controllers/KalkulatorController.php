<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalkulatorController extends Controller
{
    public function index()
    {
        return view('kalkulator');
    }

    public function store(Request $request)
    {
        $bil1 = $request->bil1;
        $bil2 = $request->bil2;
        $category = $request->category;

        if ($category == '/' && $bil2 == 0) {
            return view('kalkulator-hasil-null');
        } else {
            switch ($category) {
                case '+':
                    $hitung = $bil1 + $bil2;
                    break;
                case '-':
                    $hitung = $bil1 - $bil2;
                    break;
                case 'x':
                    $hitung = $bil1 * $bil2;
                    break;
                case '/':
                    $hitung = $bil1 / $bil2;
                    break;
            }

            return view('kalkulator-hasil', [
                'bil1' => $bil1,
                'bil2' => $bil2,
                'category' => $category,
                'hitung' => $hitung
            ]);
        }
    }
}
