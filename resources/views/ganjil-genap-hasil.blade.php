@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    <h4>
                        Fungsi Ganjil Genap
                    </h4>
                </div>
                <div class="card-body">
                    @foreach ($array as $print)
                    <p>{{$print}}</p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
