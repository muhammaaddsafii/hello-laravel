@extends('layout.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body d-flex justify-content-between">
                    <h4>
                        List Employee
                    </h4>
                    <a href="/employee/create" class="btn btn-sm btn-primary rounded-pill">Tambah Data Baru</a>
                </div>
            </div>
            <div class="card">
                <div class="card-body d-flex justify-content-center">
                <table class="table table-striped">
                <thead>
                    <tr class="text-center">
                    <th scope="col">id</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Atasan_id</th>
                    <th scope="col">Company_id</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employee as $pegawai)
                    <tr class="text-center">
                    <th scope="row">{{$pegawai->id}}</th>
                    <td>{{$pegawai->nama}}</td>
                    <td class="align-middle">{{$pegawai->atasan_id}}</td>
                    <td>{{$pegawai->company_id}}</td>
                    <td class="d-flex justify-content-center">
                        <a class="btn btn-warning btn-sm rounded-pill" href="/employee/{{$pegawai->id}}/edit">Edit</a>
                        <form action="/employee/{{$pegawai->id}}/delete" method="POST">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-sm rounded-pill">Delete</button>
                        </form>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
