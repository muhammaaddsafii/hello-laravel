@extends('layout.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body d-flex justify-content-center">
                    <h3 class="center">
                        Hello, World !
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
