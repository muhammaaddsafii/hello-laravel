@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    <h4>
                        Fungsi Ganjil Genap
                    </h4>
                </div>
                <div class="card-header d-flex justify-content-center">
                    <p class="text-secondary">
                        Disini kita akan memberikan 2 input bilangan, dan sistem akan menentukan range dari 2 bilangan tersebut apakah ganjil atau genap.
                        Contohnya : input 1 = 1, input 2 = 4, maka Outputnya :<br>
                        1 adalah bilangan ganjil <br>
                        2 adalah bilangan genap <br>
                        3 adalah bilangan ganjil <br>
                        4 adalah bilangan genap <br>
                    </p>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <form action="/ganjil-genap/store" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="bil1" class="form-label">Masukkan Bilangan Pertama :</label>
                            <input type="text" name="bil1" class="form-control" id="bil1">
                            <label for="bil2" class="form-label">Masukkan Bilangan Kedua :</label>
                            <input type="text" name="bil2" class="form-control" id="bil2">
                        </div>
                        <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
