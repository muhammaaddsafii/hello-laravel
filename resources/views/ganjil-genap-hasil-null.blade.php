@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    <h4>
                        Fungsi Ganjil Genap
                    </h4>
                </div>
                <div class="card-body d-flex justify-content-center">
                    Maaf tidak bisa dilakukan, karena Bilangan Pertama Lebih besar dibandingkan dengan Bilangan Kedua
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
