<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@index');

Route::get('/kalkulator', 'KalkulatorController@index');
Route::post('/kalkulator/store', 'KalkulatorController@store');

Route::get('/ganjil-genap', 'GanjilgenapController@index');
Route::post('/ganjil-genap/store', 'GanjilgenapController@store');

Route::get('/huruf-vocal', 'HurufvocalController@index');
Route::post('/huruf-vocal/store', 'HurufvocalController@store');


Route::get('/employee/create', 'EmployeeController@create');
Route::post('/employee/store', 'EmployeeController@store');

Route::get('/employee/{employee}/edit', 'EmployeeController@edit');
Route::patch('/employee/{employee}/edit', 'EmployeeController@update');
Route::delete('/employee/{employee}/delete', 'EmployeeController@destroy');

Route::get('/employee', 'EmployeeController@index');
